module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        'module-resolver',
        {
          alias: {
            // Make it easier to import modules. https://stackoverflow.com/a/44410874
            app: './src/app',
            lib: './src/lib',
            core: './src/core',
          }
        }
      ]
    ],
    // Read environment variables from .env files.
    // This approach is quite decent, but it doesn't play exactly like how create-react-app's .env reader does.
    // I've tried to cannibalize create-react-app's .env parsers to generate a REACT_NATIVE_APP_* equivalent in
    // babel.config.js, but all that gave me was a headache...
    // https://stackoverflow.com/a/57494657/11455106
    env: {
      production: {
        plugins: [
          [
            'inline-dotenv',
            {
              path: '.env.production'
            }
          ]
        ]
      },
      development: {
        plugins: [
          [
            'inline-dotenv',
            {
              path: '.env.development'
            }
          ]
        ]
      }
    }
  };
};
