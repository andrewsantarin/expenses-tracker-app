# expenses-tracker-app

An example monolithic project containing both client native app and server API.

- [About This Project](#about-this-project)
- [System Requirements](#system-requirements)
  - [Text Editor](#text-editor)
- [Project Setup](#project-setup)
  - [1. Install the system required tools](#1-install-the-system-required-tools)
  - [2. Clone or Download the repository](#2-clone-or-download-the-repository)
  - [3. Install the project dependencies](#3-install-the-project-dependencies)
  - [4. Prepare the database](#4-prepare-the-database)
  - [5. Setup the local environment](#5-setup-the-local-environment)
- [Running Locally](#running-locally)
  - [1. Run the server API](#1-run-the-server-api)
  - [2. Run the client app](#2-run-the-client-app)
- [TODO](#todo)

## About This Project

This is an assignment to prove the use of the React ecosystem on the frontend and the Express ecosystem on the backend for a completely JavaScript source code. Some notable key libraries & tools in use:

- typescript : type-safe language for compilation
- react : diffing view library
- react-native : native wrapper module
- react-native-router : navigation between components by path
- react-scrips + create-react-app + expo + expo-cli : created the web side first, then adapted the native boilerplate to match the webs side.
- redux : state management which can share state across multiple components
- redux-saga : asynchronous programming for more complex flow control on state changes, e.g. an API call, show loading state, return payload, throw error, etc.
- axios : http request; uses `Promise`s
- express : server-side code
- routing-controllers : controller development convenience using `@decorator()` functions
- knex : query builder

## System Requirements

- Node v10.x
- Yarn v1.x
- PostgresQL v10.6
- Expo - latest version
    - used to run the native app on a device running on iOS or Android.
- _(optional)_ Git v2.x
    - used for cloning the repository.
    - will not be used if you choose to download the repository instead.
- _(optional)_ Any PostgresQL GUI client which can accept SQL input
    - used for creating the database, its roles and its tables.
    - will not be used if you choose to insert those details through a command-line interface.
    - see [the official PostgresQL wiki](https://wiki.postgresql.org/wiki/PostgreSQL_Clients) for more GUI clients.

### Text Editor

Visual Studio Code is highly recommended.

## Project Setup

Command examples are provided for the command-line interface approach.

### 1. Install the system required tools

See [System Requirements](#system-requirements) for what you need to get.

### 2. Clone or Download the repository

> **Note**: You can also download directly from the BitBucket source. See [Downloads](https://bitbucket.org/andrewsantarin/expenses-tracker-app/downloads/) > "Download repository". Git is not needed.

```sh
git clone https://andrewsantarin@bitbucket.org/andrewsantarin/express-tracker-app.git
cd express-tracker-app
```

### 3. Install the project dependencies

```sh
yarn
```

### 4. Prepare the database

> **Note**: You can also use a GUI like pgAdmin to achieve database setup. These instructions assume that you use Terminal (macOS & Linux) or Command Prompt (Windows).

Access the database console. Here, the assumption is you are using the default superuser `postgres`. The password is also `postgres` by default.

```sh
psql -U postgres
```

Then, create a database with a name of your choice, e.g.:

- database name : `expenses_tracker`
- role name : `expenses_tracker`
- role pass : `'123'`

```sh
CREATE DATABASE expenses_tracker;
CREATE ROLE expenses_tracker PASSWORD '123';
```

Then, create the database tables as shown below. You can also find this same definition on [database.pgsql](./sql/database.pgsql) if you choose to run that file directly.

```sh
DROP TABLE IF EXISTS exp CASCADE;
CREATE TABLE exp (
  "id"        SERIAL PRIMARY KEY,
  "amount"    INTEGER NOT NULL,
  "currency"  VARCHAR(3) NOT NULL
);
```

Finally, exit.

```sh
\q
```

### 5. Setup the local environment

Expo will be used to run the app on your preferred mobile device.

Check your network settings to find your local network. The expected pattern should be `192.168.X.X`, e.g. `192.168.0.100`.

Update the [.env](./.env) file with the following format, using the database example configuration:

- database name: `expenses_tracker`
- database user: `expenses_tracker`
- database pass: `123`

```sh
PORT=9090

# Database connection details.
DATABASE_NAME=expenses_tracker
DATABASE_USER=expenses_tracker
DATABASE_PASSWORD=123
DATABASE_HOST=127.0.0.1
DATABASE_PORT=5432
```

Then apply to the [.env.development](./.env.development) file:

```dotenv
REACT_NATIVE_APP_API_BASE_URL=http://192.168.X.X:9090
```

------

Your project is now ready to run locally.

## Running Locally

### 1. Run the server API

On one terminal:

```sh
yarn server:build
yarn server:start
```

This will start the server at port `9090`, assuming you left the port number unchanged.

### 2. Run the client app

On another terminal:

```sh
yarn mobile:start
```

The terminal will present an Expo URL of this pattern: `exp://192.168.X.X:XXXXX` and a scannable QR code for you to download and load the bundled code on Expo.

## TODO

[ ] React Web support (this project was scaffolded with `create-react-app` with additional setup from `expo)

[ ] Edit expenses item

[ ] Add user login
