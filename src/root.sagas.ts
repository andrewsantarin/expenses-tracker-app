import { all, fork } from 'redux-saga/effects';

import { expenseSaga } from './app/expense/modules';

export const rootSaga = function* () {
  yield all([
    fork(expenseSaga),
  ]);
};
