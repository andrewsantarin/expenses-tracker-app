import React, { FunctionComponent } from 'react';
import { Provider } from 'react-redux';

import { App } from 'app';
import { configureStore } from './configure-store';

const store = configureStore();

export const Root: FunctionComponent<{}> = () => (
  <Provider store={store}>
    <App />
  </Provider>
);
