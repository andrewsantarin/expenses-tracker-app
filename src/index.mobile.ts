// Rewire Expo to point to this entry point.
// https://stackoverflow.com/a/54887872/11455106
import { registerRootComponent } from 'expo'; // Import this module explicitly.
import { Root } from './Root';

export default registerRootComponent(Root);
