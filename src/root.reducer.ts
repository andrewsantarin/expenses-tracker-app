import { combineReducers } from 'redux';

import { expenseReducer as expense } from 'app/expense';

export const rootReducer = combineReducers({
  expense,
});

export type RootState = ReturnType<typeof rootReducer>;
