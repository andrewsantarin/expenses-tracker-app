import { NormalizedSchema, normalize } from 'normalizr';
import { MaybeArray, KeyValuePairs } from 'lib/generics';

export const createNormalizedSchema = <E, R>(entities: E, result: R): NormalizedSchema<E, R> => ({
  entities,
  result,
});

export type NormalizerConstructor<
  T extends {} = any,
  TName extends string = '',
  TIdType extends (string | number) = number
> = {
  new (data?: MaybeArray<T>): Normalizer<T, TName, TIdType>;
  key: TName;
  schema: any;
}

export const EnhanceNormalizer = <
  T extends {} = any,
  TName extends string = '',
  TIdType extends (string | number) = number,
  C extends NormalizerConstructor<T, TName, TIdType> = NormalizerConstructor<T, TName, TIdType>,
  Meta extends object = {
    key: TName;
    schema: any;
  }
>(meta: Meta) => (target: Object | C) => {
  const { key, schema } = meta as any as {
    key: TName;
    schema: any;
  };
  (target as C).key = key;
  (target as C).schema = schema;
};

export abstract class Normalizer<
  T extends {} = any,
  TName extends string = '',
  TIdType extends (string | number) = number
> {
  data: Record<TName, T[]>;

  static key = '';
  static schema = {};

  constructor(data: MaybeArray<T> = []) {
    const key = (this.constructor as NormalizerConstructor<T, TName, TIdType>).key as TName;
    const values = Array.isArray(data) ? data : [ data ];
    this.data = {
      [key]: values,
    } as Record<TName, T[]>;
  }

  toSchema<TNormalizedSchema extends NormalizedSchema<
    Record<TName, KeyValuePairs<T>>,
    Record<TName, TIdType[]>
  >>() {
    const normalizedData = normalize(
      this.data,
      (this.constructor as NormalizerConstructor<T, TName, TIdType>).schema
    ) as TNormalizedSchema;

    return normalizedData;
  }
}
