import Knex from 'knex';
import { MaybeArray, SafePartial } from '../../generics/knex-derived';

// Adapted from https://github.com/knex/knex/issues/701#issuecomment-314818463
export function insertOrUpdate<T extends {} = any>(
  knex: Knex | Knex.Transaction,
  tableName: string,
  data: MaybeArray<SafePartial<T>>
) {
  const values = Array.isArray(data) ? data[0] : data;

  return knex.raw(
    knex(tableName)
      .insert(data)
      .toQuery() +
      ' ON DUPLICATE KEY UPDATE ' +
      Object.getOwnPropertyNames(values)
        .map(field => `${field}=VALUES(${field})`)
        .join(', ')
  );
}
