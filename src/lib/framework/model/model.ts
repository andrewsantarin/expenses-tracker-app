import Knex from 'knex';

import { Nullable, MaybeArray, SafePartial } from '../../../lib/generics';
import { DataTransferObject } from './types';

export type ModelMeta<M extends {} = {}> = {
  dbTable: string;
  ordering?: (keyof DataTransferObject<M>)[];
  primaryKey?: keyof DataTransferObject<M>;
};

export const createModelMeta = <M extends {}>({
  dbTable = '',
  ordering: order = [],
  primaryKey,
}: Partial<ModelMeta<M>> = {}) => {
  const meta = {
    dbTable,
    order,
    primaryKey,
  };

  return Object.freeze(meta);
};

export type ModelDatabase<M extends {} = {}> =
  | Knex.Transaction<M, M[]>
  | Knex<M>;

export type ModelValues<M> = SafePartial<DataTransferObject<M>>;
export type ModelValuesMaybeArray<M> = MaybeArray<ModelValues<M>>;

export type ModelConstructor<M extends {} = {}> = {
  new(values?: ModelValues<M>): Model<M>;
  meta: ModelMeta<M>;
  fields: {
    [key in keyof M]: any;
  };
};

export class ModelState<M extends {} = {}> {
  protected _columns: Partial<Record<keyof M, true>> = {};
  protected _initialized = false;

  get columns() {
    return Object.keys(this._columns) as (keyof M)[];
  }

  addColumn(column: keyof M) {
    this._columns[column] = true;
  }

  removeColumn(column: keyof M) {
    delete this._columns[column];
  }

  resetColumns() {
    this._columns = {};
  }

  get initialized() {
    return this._initialized;
  }

  initialize() {
    this._initialized = true;
  }

  resetInitialized() {
    this._initialized = false;
  }
}

export type ModelInstance<
  M extends {} = {},
  S extends ModelState<M> = ModelState<M>
  > = {
    _state: S;
    toJSON(): DataTransferObject<M>;
  };

export abstract class Model<M extends {} = {}, MPrimarKey extends keyof M & string = any> {
  static fields = {};
  static meta: Readonly<ModelMeta> = createModelMeta();

  private _state = new ModelState<M>();

  constructor(values: ModelValues<M> = {} as ModelValues<M>) {
    Object.keys(values).forEach((key) => {
      this[key as keyof Model<M>] = values[key as keyof typeof values] as any;
    });

    return new Proxy<Model<M>>(this, {
      set: (target, key, value) => {
        target[key as keyof Model<M>] = value;

        type K = keyof ModelValues<M>;

        if (
          true &&
          !this._state.initialized &&
          value !== values[key as K]
        ) {
          this._state.initialize();
        }

        if (
          key in (this.constructor as ModelConstructor<M>).fields &&
          this._state.initialized &&
          value !== values[key as K]
        ) {
          this._state.addColumn(key as keyof M);
        }

        return true;
      }
    });
  }

  static init<M>(this: { new(): Model<M>; }) {
    const Model = this as ModelConstructor<M>;

    return {
      single(values: ModelValuesMaybeArray<M>) {
        const value = Array.isArray(values) ? values[0] : values;

        if (value == null) {
          return;
        }

        return new Model(value) as any as M;
      },
      multiple(values: ModelValuesMaybeArray<M>) {
        const valuesArray = Array.isArray(values) ? values : [values];

        return valuesArray.map(values => new Model(values) as any as M);
      },
    };
  }

  static objects<M>(this: { new(): Model<M>; }, database: ModelDatabase<DataTransferObject<M>>) {
    type MKey = keyof DataTransferObject<M> & string;
    type MConstructor = ModelConstructor<M>;
    type MValues = ModelValues<M>;
    type MReturning = MKey | MKey[] | '*';
    const defaultValues = {} as MValues;
    const defaultReturning: MReturning = '*';
    const { dbTable, primaryKey } = (this as MConstructor).meta as Required<ModelMeta<M>>;

    return {
      create(
        values: MValues = defaultValues,
        returning: MReturning = defaultReturning
      ) {
        return database(dbTable)
          .insert(values)
          .returning(returning);
      },
      update(
        values: MValues = defaultValues,
        object: MValues = defaultValues
      ) {
        return database(dbTable)
          .where(object)
          .update(values);
      },
      delete(
        criteria: MValues = defaultValues
      ) {
        return database(dbTable)
          .where(criteria)
          .delete();
      },
      filter(
        values: MValues = defaultValues,
        returning: MReturning = defaultReturning
      ) {
        return database(dbTable)
          .where(values)
          .select(returning);
      },
      filterByPk(
        id: string | number,
        returning: MReturning = defaultReturning
      ) {
        const criteria = { [primaryKey]: id };
        return database(dbTable)
          .where(criteria)
          .select(returning);
      },
    };
  }

  instance(database: ModelDatabase<M>) {
    type MKey = keyof DataTransferObject<M> & string;
    type MData = DataTransferObject<M>;
    type MConstructor = ModelConstructor<M>;
    const { meta } = this.constructor as MConstructor;
    const { dbTable, primaryKey } = meta as Required<ModelMeta<M>>;
    const criteria = { [primaryKey]: (this as any)[primaryKey] };
    const instance = this;

    const resetState = (records: MData[], obj: Knex.Sql, builder: Knex.QueryBuilder<MData>) => {
      const [ record ] = records;
      (instance as any)[primaryKey as MKey] = record[primaryKey as MKey];
      ((instance as any) as ModelInstance<M>)._state.resetColumns();
      ((instance as any) as ModelInstance<M>)._state.resetInitialized();
    };

    return {
      save() {
        // Falsy primary key values indicate that the column is empty.
        // Specifically: `false`, `null`, `undefined` `0`, `""`
        const primaryKeyValue: Nullable<M[keyof M]> | undefined = (instance as any)[primaryKey as MKey];
        if (!primaryKeyValue) {
          // "INSERT" statement
          const values = instance.toJSON();
          return database(dbTable)
            .insert((instance.toJSON() as any) as SafePartial<M>)
            .returning('*')
            .on('query-response', resetState);
        } else {
          // "UPDATE" statement
          const values = instance._state.columns.reduce<MData>(
            (lookup, key) => {
              (lookup as any)[key] = ((instance as any) as M)[key];
              return lookup;
            },
            {} as any
          );
          return database(dbTable)
            .where({ [primaryKey]: primaryKeyValue })
            .update((values as any) as SafePartial<M>)
            .returning('*')
            .on('query-response', resetState);
        }
      },
      delete() {
        return database(dbTable)
          .where(criteria)
          .delete();
      },
    };
  };

  toJSON(): DataTransferObject<M> {
    // Read database fields only. Anything else is considered not part of the data transfer object.
    const json = Object.keys((this.constructor as ModelConstructor<M>).fields).reduce((lookup, key) => {
      (lookup as any)[key] = this[key as keyof this];

      return lookup;
    }, {} as DataTransferObject<M>);

    return json;
  }
}
