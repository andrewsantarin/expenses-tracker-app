import { ModelConstructor } from './model';

export type ErrorMessages = {
  null?: string;
  required?: string;
  invalid?: string;
  invalidChoice?: string;
};

export type FieldOptionsBase<T, ColumnType extends string, FieldOptions extends {} = {}> = {
  type?: ColumnType;
} & {
  dbColumn?: string;
  null?: boolean;
  required?: boolean;
  options?: T[];
  default?: T;
  helpText?: string;
  errorMessages?: ErrorMessages;
} & FieldOptions;
export type IntegerFieldOptions = FieldOptionsBase<
  number,
  | 'int'
  | 'int2'
  | 'int4'
  | 'int8'
  | 'integer'
  | 'tinyint'
  | 'smallint'
  | 'mediumint'
  | 'bigint',
  {
    min?: number;
    max?: number;
  }
>;
export type FloatFieldOptions = FieldOptionsBase<
  number,
  | 'float'
  | 'float4'
  | 'float8'
  | 'double'
  | 'dec'
  | 'decimal'
  | 'fixed'
  | 'numeric'
  | 'real'
  | 'double precision'
  | 'number',
  {
    min?: number;
    max?: number;
  }
>;
export type CharFieldOptions = FieldOptionsBase<
  string,
  | 'character varying'
  | 'varying character'
  | 'char varying'
  | 'nvarchar'
  | 'national varchar'
  | 'character'
  | 'native character'
  | 'varchar'
  | 'char'
  | 'nchar'
  | 'national char'
  | 'varchar2'
  | 'nvarchar2'
  | 'raw'
  | 'binary'
  | 'varbinary'
  | 'string',
  {
    minLength?: number;
    maxLength?: number;
    trim?: boolean;
  }
>;
export type TextFieldOptions = FieldOptionsBase<
  string,
  | 'tinytext'
  | 'mediumtext'
  | 'blob'
  | 'text'
  | 'ntext'
  | 'citext'
  | 'longtext'
  | 'linestring'
  | 'multilinestring',
  {
    minLength?: number;
    maxLength?: number;
    trim?: boolean;
  }
>;
export type EmailFieldOptions = FieldOptionsBase<
  string,
  | 'character varying'
  | 'varying character'
  | 'char varying'
  | 'nvarchar'
  | 'national varchar'
  | 'character'
  | 'native character'
  | 'varchar'
  | 'char'
  | 'nchar'
  | 'national char'
  | 'varchar2'
  | 'nvarchar2'
  | 'raw'
  | 'binary'
  | 'varbinary'
  | 'string'
  | 'tinytext'
  | 'mediumtext'
  | 'blob'
  | 'text'
  | 'ntext'
  | 'citext'
  | 'longtext'
  | 'linestring'
  | 'multilinestring',
  {
    minLength?: number;
    maxLength?: number;
    trim?: boolean;
  }
>;
export type PhoneFieldOptions = FieldOptionsBase<
  string,
  | 'character varying'
  | 'varying character'
  | 'char varying'
  | 'nvarchar'
  | 'national varchar'
  | 'character'
  | 'native character'
  | 'varchar'
  | 'char'
  | 'nchar'
  | 'national char'
  | 'varchar2'
  | 'nvarchar2'
  | 'raw'
  | 'binary'
  | 'varbinary'
  | 'string'
  | 'tinytext'
  | 'mediumtext'
  | 'blob'
  | 'text'
  | 'ntext'
  | 'citext'
  | 'longtext'
  | 'linestring'
  | 'multilinestring',
  {
    minLength?: number;
    maxLength?: number;
    trim?: boolean;
  }
>;
export type DateFieldOptions = FieldOptionsBase<
  Date,
  | 'datetime'
  | 'datetime2'
  | 'datetimeoffset'
  | 'time'
  | 'time with time zone'
  | 'time without time zone'
  | 'timestamp'
  | 'timestamp without time zone'
  | 'timestamp with time zone'
  | 'timestamp with local time zone'
  | 'timetz'
  | 'timestamptz',
  {
    type: 'date';
  }
>;
export type PrimaryKeyFieldOptions = FieldOptionsBase<
  number,
  'serial',
  {
  }
>;

export const Field = <O extends {} = {}>(options?: any) => (
  obj: Object,
  prop: string
) => {
  (obj.constructor as ModelConstructor<O>).fields[prop as keyof O] = options;
};

export const PrimaryKeyField = <O extends {} = {}>(options?: PrimaryKeyFieldOptions) => (
  obj: Object,
  prop: string
) => {
  (obj.constructor as ModelConstructor<O>).fields[prop as keyof O] = options;
};

export const IntegerField = <O extends {} = {}>(options?: IntegerFieldOptions) => (
  obj: Object,
  prop: string
) => {
  (obj.constructor as ModelConstructor<O>).fields[prop as keyof O] = options;
};

export const FloatField = <O extends {} = {}>(options?: FloatFieldOptions) => (
  obj: Object,
  prop: string
) => {
  (obj.constructor as ModelConstructor<O>).fields[prop as keyof O] = options;
};

export const CharField = <O extends {} = {}>(options?: CharFieldOptions) => (
  obj: Object,
  prop: string
) => {
  (obj.constructor as ModelConstructor<O>).fields[prop as keyof O] = options;
};

export const TextField = <O extends {} = {}>(options?: TextFieldOptions) => (
  obj: Object,
  prop: string
) => {
  (obj.constructor as ModelConstructor<O>).fields[prop as keyof O] = options;
};

export const EmailField = <O extends {} = {}>(options?: EmailFieldOptions) => (
  obj: Object,
  prop: string
) => {
  (obj.constructor as ModelConstructor<O>).fields[prop as keyof O] = options;
};

export const PhoneField = <O extends {} = {}>(options?: PhoneFieldOptions) => (
  obj: Object,
  prop: string
) => {
  (obj.constructor as ModelConstructor<O>).fields[prop as keyof O] = options;
};
