export type DataPropertyNames<T> = {
  [K in keyof T]: T[K] extends Function ? never : K;
}[keyof T];

export type DataPropertiesOnly<T> = {
  [P in DataPropertyNames<T>]: T[P] extends object ? DataTransferObject<T[P]> : T[P];
};

export type DataTransferObject<T> = DataPropertiesOnly<T>;
