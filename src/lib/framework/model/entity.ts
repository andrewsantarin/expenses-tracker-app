import { ModelMeta, ModelConstructor } from './model';

export const Entity = <
  M extends {} = {},
  C extends ModelConstructor<M> = ModelConstructor<M>
>(
  meta: Partial<ModelMeta<M>> = {}
) => (target: Object | C) => {
  const { dbTable = (target as C).name.toLowerCase(), ordering = [], primaryKey } = meta;
  const { fields } = target as C;
  (target as C).fields = fields ?? {};
  (target as C).meta = {
    dbTable,
    ordering,
    primaryKey,
  };
};
