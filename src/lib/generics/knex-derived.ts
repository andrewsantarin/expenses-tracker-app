export type SafePartial<T> = T extends {} ? Partial<T> : any;
export type MaybeArray<T> = T | T[];
