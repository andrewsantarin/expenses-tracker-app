// https://stackoverflow.com/a/41870915
export type KeyValuePairs<Value> = {
  [key: string]: Value;
};

// https://alexjover.com/blog/typescript-lookup-types-type-safe-properties/
// https://stackoverflow.com/a/50396312
export type Keys<T> = keyof T;

// https://stackoverflow.com/a/49286056
export type Values<T> = T[keyof T];
