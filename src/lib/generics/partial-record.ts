/**
 * Construct a type with a set of **optional** properties `K` of type `T`.
 * 
 * @see https://stackoverflow.com/a/53276873/11455106
 */
export type PartialRecord<K extends keyof any, T> = Partial<Record<K, T>>;
