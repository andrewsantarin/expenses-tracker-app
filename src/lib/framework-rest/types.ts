export type ApiError<T extends Error | object = {}> = T;
