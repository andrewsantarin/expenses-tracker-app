export const createPrefixedActionType = (type: string, prefix: string): string => `${prefix}/${type}`;
