import { ApiState } from './create-api-state';

export type ApiStateService<S extends ApiState> = {
  trigger(): S;
  request(): S;
  success(): S;
  failure(error: any): S;
  fulfill(): S;
  aborted(): S;
  update(): S;
};

export const createApiStateService = <S extends ApiState>(state: S): ApiStateService<S> => {
  const apiStateService: ApiStateService<S> = {
    trigger(): S {
      return {
        ...state
      };
    },
    request(): S {
      return {
        ...state,
        isFetching: true
      };
    },
    success(): S {
      return {
        ...state,
        isFetching: false,
        error: undefined,
        lastUpdated: new Date()
      };
    },
    failure(error: any): S {
      return {
        ...state,
        isFetching: false,
        error: error,
        lastUpdated: new Date()
      };
    },
    fulfill(): S {
      return {
        ...state
      };
    },
    aborted(): S {
      return {
        ...state,
        isFetching: false
      };
    },
    update(): S {
      return {
        ...state,
        error: undefined,
        lastUpdated: new Date()
      };
    }
  };

  return apiStateService;
};
