import { createAction } from 'deox';
import isFunction from 'lodash/isFunction';

import { createPrefixedActionType } from './create-prefixed-action-type';

type MapPayload<Payload, TransformedPayload> = (payload: Payload) => TransformedPayload;
type LifecycleActionPayloadMappers<
  TriggerPayload, TriggerTransformedPayload,
  RequestPayload, RequestTransformedPayload,
  SuccessPayload, SuccessTransformedPayload,
  FailurePayload, FailureTransformedPayload,
  FulfillPayload, FulfillTransformedPayload,
  AbortedPayload, AbortedTransformedPayload,
> = Partial<{
  onTrigger: MapPayload<TriggerPayload, TriggerTransformedPayload>;
  onRequest: MapPayload<RequestPayload, RequestTransformedPayload>;
  onSuccess: MapPayload<SuccessPayload, SuccessTransformedPayload>;
  onFailure: MapPayload<FailurePayload, FailureTransformedPayload>;
  onFulfill: MapPayload<FulfillPayload, FulfillTransformedPayload>;
  onAborted: MapPayload<AbortedPayload, AbortedTransformedPayload>;
}>;

/**
 * Builds a singleton of Flux Standard (`deox`) Actions representing an entire asynchronous lifecycle.
 *
 * The singleton contains actions which represent events in each lifecycle. These event lifecycles are
 * commonly used in side-effect patterns such as `redux-saga` and `redux-observable`, where firing one
 * specific event triggers a series of events asynchronously.
 *
 * Each singleton includes these lifecycle actions and their recommended use cases:
 * - `trigger`: Used as a listener, ideally to begin the entire lifecycle.
 * - `request`: Used as an indication that the lifecycle is running.
 * - `success`: Used as an indication that the lifecycle has executed with returned values.
 * - `failure`: Used as an indication that the lifecycle has executed with errors.
 * - `fulfill`: Used to clean up the lifecycle, usually after a successful execution.
 * - `aborted`: Used to clean up the lifecycle, usually after a failed execution.
 *
 * Each singleton is also an alias to its `.trigger` property. You can invoke the created singleton
 * directly without having to explicitly call its `.trigger` function property.
 *
 * @see https://github.com/smeijer/redux-define — Original design pattern and inspiration.
 * @see https://stackoverflow.com/a/41853194 — Aliasing a function to an object in TypeScript.
 *
 * @param {string} lifecyclePrefix A prefix for every lifecycle action so that they are easily identified.
 * @param {LifecycleActionPayloadMappers<
 *     TriggerPayload, TriggerTransformedPayload,
 *     RequestPayload, RequestTransformedPayload,
 *     SuccessPayload, SuccessTransformedPayload,
 *     FailurePayload, FailureTransformedPayload,
 *     FulfillPayload, FulfillTransformedPayload,
 *     AbortedPayload, AbortedTransformedPayload
 *   >} [payloadMappers={}]
 *
 * @returns A singleton representing all of the possible actions within a lifecycle.
 */
export const createLifecycleActions = <
  TriggerPayload, TriggerTransformedPayload,
  RequestPayload, RequestTransformedPayload,
  SuccessPayload, SuccessTransformedPayload,
  FailurePayload, FailureTransformedPayload,
  FulfillPayload, FulfillTransformedPayload,
  AbortedPayload, AbortedTransformedPayload,
  Meta
>(
  lifecyclePrefix: string,
  payloadMappers: LifecycleActionPayloadMappers<
    TriggerPayload, TriggerTransformedPayload,
    RequestPayload, RequestTransformedPayload,
    SuccessPayload, SuccessTransformedPayload,
    FailurePayload, FailureTransformedPayload,
    FulfillPayload, FulfillTransformedPayload,
    AbortedPayload, AbortedTransformedPayload
  > = {}
) => {
  const {
    onTrigger,
    onRequest,
    onSuccess,
    onFailure,
    onFulfill,
    onAborted,
  } = payloadMappers;

  const lifecycleActions = {
    trigger: createAction(
      createPrefixedActionType('TRIGGER', lifecyclePrefix),
      resolve => (payload: TriggerPayload, meta: Meta) =>
        resolve(isFunction(onTrigger) ? onTrigger(payload) : payload as any as TriggerTransformedPayload, meta)
    ),
    request: createAction(
      createPrefixedActionType('REQUEST', lifecyclePrefix),
      resolve => (payload: RequestPayload, meta: Meta) =>
        resolve(isFunction(onRequest) ? onRequest(payload) : payload as any as RequestTransformedPayload, meta)
    ),
    success: createAction(
      createPrefixedActionType('SUCCESS', lifecyclePrefix),
      resolve => (payload: SuccessPayload, meta: Meta) =>
        resolve(isFunction(onSuccess) ? onSuccess(payload) : payload as any as SuccessTransformedPayload, meta)
    ),
    failure: createAction(
      createPrefixedActionType('FAILURE', lifecyclePrefix),
      resolve => (payload: FailurePayload, meta: Meta) =>
        resolve(isFunction(onFailure) ? onFailure(payload) : payload as any as FailureTransformedPayload, meta)
    ),
    fulfill: createAction(
      createPrefixedActionType('FULFILL', lifecyclePrefix),
      resolve => (payload: FulfillPayload, meta: Meta) =>
        resolve(isFunction(onFulfill) ? onFulfill(payload) : payload as any as FulfillTransformedPayload, meta)
    ),
    aborted: createAction(
      createPrefixedActionType('ABORTED', lifecyclePrefix),
      resolve => (payload: AbortedPayload, meta: Meta) =>
        resolve(isFunction(onAborted) ? onAborted(payload) : payload as any as AbortedTransformedPayload, meta)
    )
  };

  // Set the `.trigger` property as the singleton's main function.
  // Invoking the singleton is the same as invoking `trigger()`.
  // This means `.trigger` can be called without having to access the `.trigger` property explicity.
  // https://stackoverflow.com/a/41853194
  let finalLifecycleActions = Object.assign(
    lifecycleActions.trigger,
    lifecycleActions
  );

  return finalLifecycleActions;
};
