export type ApiState = {
  isFetching: boolean;
  error?: any;
  lastUpdated?: Date;
};

export const createApiState = (): ApiState => {
  return {
    isFetching: false
  };
};
