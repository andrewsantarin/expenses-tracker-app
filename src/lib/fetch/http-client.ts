import axios, { AxiosResponse, AxiosRequestConfig } from 'axios';

export const HttpClientBaseURL = (baseURL: string) => `${process.env.REACT_NATIVE_APP_API_BASE_URL || ''}${baseURL}`;

export type HttpClientResponseOptions<T extends any = {}> = {
  onFulfilled?: (value: AxiosResponse<T>) => AxiosResponse<T> | Promise<AxiosResponse<T>>;
  onRejected?: (error: any) => any;
};

export const HttpClient = <T extends any>(
  config: AxiosRequestConfig = {},
  onResponse: HttpClientResponseOptions<T> = {}
) => {
  const { baseURL = '', ...rest } = config;
  const httpClientConfig = { baseURL: HttpClientBaseURL(baseURL), ...rest };
  const httpClient = axios.create(httpClientConfig);

  if (onResponse) {
    httpClient.interceptors.response.use(
      onResponse.onFulfilled,
      onResponse.onRejected,
    );
  }

  return httpClient;
};
