import knex from 'knex';
import pg from 'pg';

import './env';

// Configure node-postgres to parse `NUMERIC` columns.
// If more data types need to be parsed, add them here.
// https://stackoverflow.com/a/57210556/11455106
pg.types.setTypeParser(pg.types.builtins.INT2, (value: string) => {
  return parseInt(value);
});

pg.types.setTypeParser(pg.types.builtins.INT4, (value: string) => {
  return parseInt(value);
});

pg.types.setTypeParser(pg.types.builtins.INT8, (value: string) => {
  return parseInt(value);
});

pg.types.setTypeParser(pg.types.builtins.FLOAT8, (value: string) => {
  return parseFloat(value);
});

pg.types.setTypeParser(pg.types.builtins.NUMERIC, (value: string) => {
  return parseFloat(value);
});

export const database = knex({
  client: 'pg',
  connection: {
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    debug: process.env.NODE_ENV === 'development',
  },
});
