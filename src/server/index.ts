import 'reflect-metadata';
import express from 'express';
import morgan from 'morgan';
import { useExpressServer } from 'routing-controllers';

import './env';
import { controllers } from './controllers';

const app = express();
const port = parseInt(process.env.PORT || '9090');

// Log incoming requests.
app.use(morgan('dev'));

// Parse incoming json, form, xml requests.
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Use routing-controllers.
useExpressServer(app, {
  controllers
});

app.listen(port);
