import { RequestHandler } from 'express';

export const handleApiErrors: RequestHandler = (req, res, next) => {
  return next();
};
