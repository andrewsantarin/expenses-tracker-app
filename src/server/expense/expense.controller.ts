import {
  JsonController,
  Body,
  Post,
  Get,
  Param,
  Patch,
  Delete,
  UseAfter,
  HttpCode
} from 'routing-controllers';

import { ExpenseValues } from '../../core/expense';
import { handleApiErrors } from '../api';

import {
  createUserExpense,
  updateUserExpense,
  deleteUserExpense,
  filterUserExpenseByUsername,
  filterUserExpenseById
} from './expense.services';

@JsonController('/expenses')
@UseAfter(handleApiErrors)
export class ExpenseController {
  @Get('/')
  list() {
    return filterUserExpenseByUsername();
  }

  @Post('/')
  @HttpCode(201)
  create(
    @Body() data: ExpenseValues
  ) {
    return createUserExpense(data);
  }

  @Get('/:id')
  retrieve(
    @Param('id') id: number
  ) {
    return filterUserExpenseById(id);
  }

  @Patch('/:id')
  update(
    @Param('id') id: number,
    @Body() data: ExpenseValues
  ) {
    return updateUserExpense(data, id);
  }

  @Delete('/:id')
  delete(
    @Param('id') id: number
  ) {
    return deleteUserExpense(id);
  }
}
