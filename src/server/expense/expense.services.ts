import { database } from '../database';

import { ExpenseValues } from '../../core/expense';
import { Expense } from './expense.models';

export const filterUserExpenseByUsername = async () => {
  const expenses = await Expense.objects(database)
    .filter()
    .orderBy('id', 'desc')
    .then(Expense.init().multiple);

  return expenses;
};

export const filterUserExpenseById = async (id: number) => {
  const expense = await Expense.objects(database)
    .filter({ id })
    .then(Expense.init().single);

  return expense;
}

export const createUserExpense = async (values: ExpenseValues) => {
  const { amount, currency } = values;
  const expense = new Expense({
    amount,
    currency,
  });
  await expense.instance(database).save();

  return expense;
};

export const updateUserExpense = async (values: ExpenseValues, id: number) => {
  const expense = await Expense.objects(database)
    .update(values)
    .where({ id })
    .returning('*')
    .then(Expense.init().single);

  return expense;
};

export const deleteUserExpense = async (id: number) => {
  const count = await Expense.objects(database).delete({ id });

  return count;
};
