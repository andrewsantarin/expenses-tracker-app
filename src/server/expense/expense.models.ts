import { Model, Entity, PrimaryKeyField, CharField, IntegerField } from '../../lib/framework/model';
import { Expense as ExpenseObject } from '../../core/expense';

@Entity<ExpenseObject>({
  dbTable: 'exp',
  primaryKey: 'id',
})
export class Expense extends Model<Expense> implements ExpenseObject {
  @PrimaryKeyField()
  id!: number;

  @CharField({

  })
  currency!: string;

  @IntegerField({

  })
  amount!: number;
}
