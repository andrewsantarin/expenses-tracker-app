import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import { Expense } from 'core/expense';
import { ExpenseDeleteButtonContainer } from './ExpenseDeleteButtonContainer';

export const ExpensesListItem: React.FunctionComponent<Expense> = ({
  id,
  currency,
  amount: value,
}) => {
  const amount = (value / 100).toLocaleString('en-US', {
    style: 'currency',
    currency: currency,
  });

  return (
    <View style={styles.item}>
      <Text style={styles.text}>
        {amount}
      </Text>
      <ExpenseDeleteButtonContainer
        id={id}
        title="[-] Delete"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    backgroundColor: '#ACCEEE',
  },
  text: {
    fontSize: 16,
    textAlign: 'right',
  },
});
