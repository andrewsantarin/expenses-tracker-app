import React, { Component } from 'react';
import { SafeAreaView, FlatList, StyleSheet, View, Text } from 'react-native';

import { Expense } from 'core/expense';

import { ExpensesListItem } from './ExpensesListItem';
import { Link } from 'react-router-native';
import { LoadingBar } from 'app/components/loading-bar/LoadingBar';

export type ExpensesListProps = Partial<{
  expenses: Expense[];
  isFetching: boolean;
  error: any;
  lastUpdated: Date;
  listExpenses: () => void;
}>;
export type ExpensesListState = {};

export class ExpensesList extends Component<ExpensesListProps, ExpensesListState> {
  componentDidMount() {
    const { listExpenses = () => {} } = this.props;
    listExpenses();
  }

  render() {
    const {
      expenses = null,
      isFetching = false,
      listExpenses
    } = this.props;

    return (
      <SafeAreaView>
        {expenses && expenses.length
          ? (
            <FlatList<Expense>
              data={expenses}
              renderItem={({ item }) => <ExpensesListItem {...item} />}
              keyExtractor={({ id }) => id.toString()}
              refreshing={isFetching}
              onRefresh={listExpenses}
            />
          )
          : (
            <View style={styles.emptyContainer}>
              <Text>It's empty.</Text>
              <Text>Nothing to see here!</Text>
              <View>
                <Link
                  to="/add"
                  underlayColor="#f0f4f7"
                  style={styles.addLink}
                >
                  <Text style={styles.addLinkText}>+ Add new item</Text>
                </Link>
              </View>
            </View>
          )
        }
        {isFetching && (
          <LoadingBar title="loading..." />
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  emptyContainer: {
    padding: 20,
    alignItems: 'center',
  },
  addLink: {
    padding: 30
  },
  addLinkText: {
    color: 'blue',
  },
});
