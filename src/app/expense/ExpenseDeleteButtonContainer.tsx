import { ButtonProps } from 'react-native';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';

import { RootState } from 'root.reducer';
import { deleteExpense } from './modules/expense.actions';
import { ExpenseDeleteButton, ExpenseDeleteButtonProps } from './ExpenseDeleteButton';

type ExpenseAddFormStateProps = {

};
type ExpenseAddFormDispatchProps = Pick<ButtonProps, 'onPress'>;

const mapStateToProps: MapStateToProps<ExpenseAddFormStateProps, ExpenseDeleteButtonProps, RootState> = () => ({

});
const mapDispatchToProps: MapDispatchToProps<ExpenseAddFormDispatchProps, ExpenseDeleteButtonProps> = (dispatch, props) => ({
  onPress: () => {
    const { id } = props;
    if (!id) return;
    dispatch(deleteExpense.trigger(null, { id }));
  }
});

export const ExpenseDeleteButtonContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpenseDeleteButton);
