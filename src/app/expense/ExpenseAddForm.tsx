import React, { useState } from 'react';
import { Button, View, Text, StyleSheet, SafeAreaView, TouchableWithoutFeedback, Keyboard } from 'react-native';

import { Expense } from 'core/expense';

import { CurrencyInput, CurrencyInputProps } from 'app/components/currency-input';
import { LoadingBar } from 'app/components/loading-bar/LoadingBar';

export type ExpenseAddFormProps = Partial<Pick<CurrencyInputProps, 'currency' | 'currencyDisplay' | 'min' | 'max'> & {
  submitting: boolean;
  onSubmit: (values: Partial<Expense>) => void;
}>;

export const ExpenseAddForm: React.FC<ExpenseAddFormProps> = ({
  min,
  max,
  currency = 'MYR',
  currencyDisplay,
  submitting = false,
  onSubmit = () => {},
}) => {
  const [ amount, setAmount ] = useState(0);
  const handleChange = (value: number) => {
    setAmount(value);
  };
  const handlePress = () => {
    onSubmit({
      currency,
      amount,
    });
  };

  return (
    <SafeAreaView>
      <TouchableWithoutFeedback
        onPress={Keyboard.dismiss}
        accessible={false}
      >
        <View style={styles.container}>
          <View>
            <Text style={styles.label}>Enter an amount below!</Text>
            <CurrencyInput
              currency={currency}
              currencyDisplay={currencyDisplay}
              min={min}
              max={max}
              onValueChange={handleChange}
              value={amount}
              style={styles.input}
            />
            <Button
              onPress={handlePress}
              title="Submit"
            />
          </View>
          {submitting && (
            <LoadingBar title="Submitting..." />
          )}
        </View>
      </TouchableWithoutFeedback>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    fontSize: 25,
    fontWeight: '700',
  },
  input: {
    backgroundColor: 'white',
    borderColor: 'gray',
    borderWidth: 1,
    fontSize: 20,
    marginBottom: 20,
    marginTop: 20,
    padding: 20,
    textAlign: 'right',
    width: 300,
  },
});
