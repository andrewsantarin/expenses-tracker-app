import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';

import { RootState } from 'root.reducer';
import { listExpenses } from './modules/expense.actions';
import { ExpensesList, ExpensesListProps } from './ExpensesList';

export type ExpenseListStateProps = Pick<ExpensesListProps, 'expenses' | 'isFetching' | 'error' | 'lastUpdated'>;
export type ExpensesListDispatchProps = Required<Pick<ExpensesListProps, 'listExpenses'>>;

const mapStateToProps: MapStateToProps<ExpenseListStateProps, ExpensesListProps, RootState> = (state) => {
  const { expense } = state;
  const { api, schema } = expense;
  const { isFetching, error, lastUpdated } = api;
  const { entities, result } = schema;
  const expenses = result.expenses.map((id) => entities.expenses[id]);

  return {
    expenses,
    isFetching,
    error,
    lastUpdated,
  };
};

const mapDispatchToProps: MapDispatchToProps<ExpensesListDispatchProps, ExpensesListProps> = (dispatch) => {
  return {
    listExpenses: () => dispatch(listExpenses(null, null)),
  };
};

export const ExpensesListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpensesList);
