import React, { Fragment } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { Link } from 'react-router-native';

import { Expense } from 'core/expense';

import { RootState } from 'root.reducer';
import { createExpense, resetCreated } from './modules/expense.actions';
import { ExpenseAddForm as ExpenseAddForm, ExpenseAddFormProps } from './ExpenseAddForm';
import { useUnmount } from 'lib/hooks/useUnmount';

type ExpenseAddFormStateProps = Partial<{
  submitting: boolean;
  created: boolean;
}>;
type ExpenseAddFormDispatchProps = {
  createExpense: (payload: Partial<Expense>) => void;
  resetCreated: () => void;
};

const mapStateToProps: MapStateToProps<ExpenseAddFormStateProps, ExpenseAddFormProps, RootState> = (state) => ({
  submitting: state.expense.api.isFetching,
  created: state.expense.ui.created,
});
const mapDispatchToProps: MapDispatchToProps<ExpenseAddFormDispatchProps, ExpenseAddFormProps> = (dispatch) => ({
  createExpense: (payload) => dispatch(createExpense.trigger(payload, null)),
  resetCreated: () => dispatch(resetCreated()),
});

export const ExpenseAddFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(({
  createExpense,
  created,
  resetCreated,
  submitting
}: ExpenseAddFormProps & ExpenseAddFormStateProps & ExpenseAddFormDispatchProps) => {
  useUnmount(() => {
    resetCreated();
  });

  return (
    <Fragment>
      {created && (
        <View  style={styles.statusContainer}>
          <Text>
            Item added!
          </Text>
          <Link to="/">
            <Text style={styles.statusListLinkText}>See on list -></Text>
          </Link>
        </View>
      )}
      <ExpenseAddForm
        currency="MYR"
        currencyDisplay="symbol"
        submitting={submitting}
        onSubmit={createExpense}
      />
    </Fragment>
  );
});

const styles = StyleSheet.create({
  statusContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
  },
  statusListLinkText: {
    marginLeft: 4,
    color: 'blue',
  },
});
