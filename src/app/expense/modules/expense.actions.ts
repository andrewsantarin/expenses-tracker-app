import { createAction } from 'deox';

import { createLifecycleActions } from 'lib/fetch-state';
import { ApiError } from 'lib/framework-rest';

import { Expense } from 'core/expense';

import { ExpenseNormalizer, ExpenseNormalizedSchema } from './expense.schema';

export const listExpenses = createLifecycleActions<
  null, null,
  null, null,
  Expense[], ExpenseNormalizedSchema,
  ApiError, ApiError,
  null, null,
  null, null,
  null
>(
  '@@expense/LIST',
  {
    onSuccess: (payload) => new ExpenseNormalizer(payload).toSchema(),
  }
);

export const createExpense = createLifecycleActions<
  Partial<Expense>, Partial<Expense>,
  null, null,
  Expense, Expense,
  ApiError, ApiError,
  null, null,
  null, null,
  null
>(
  '@@expense/CREATE'
);

export const retrieveExpense = createLifecycleActions<
  null, null,
  null, null,
  Expense, Expense,
  ApiError, ApiError,
  null, null,
  null, null,
  Pick<Expense, 'id'>
>(
  '@@expense/RETRIEVE'
);

export const updateExpense = createLifecycleActions<
  null, null,
  null, null,
  Expense, Expense,
  ApiError, ApiError,
  null, null,
  null, null,
  Pick<Expense, 'id'>
>(
  '@@expense/UPDATE'
);

export const deleteExpense = createLifecycleActions<
  null, null,
  null, null,
  number, boolean,
  ApiError, ApiError,
  null, null,
  null, null,
  Pick<Expense, 'id'>
>(
  '@@expense/DELETE',
  {
    onSuccess: Boolean,
  }
);

export const resetCreated = createAction(
  '@@expense/RESET_CREATED',
  resolve => () => resolve()
);

export const resetUpdated = createAction(
  '@@expense/RESET_UPDATED',
  resolve => () => resolve()
);

export const resetDeleted = createAction(
  '@@expense/RESET_DELETED',
  resolve => () => resolve()
);
