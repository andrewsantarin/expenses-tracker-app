import { schema, NormalizedSchema } from 'normalizr';

import { KeyValuePairs } from 'lib/generics';
import { EnhanceNormalizer, Normalizer } from 'lib/schema';

import { Expense } from 'core/expense';

export type ExpenseNormalizedSchema = NormalizedSchema<
  {
    expenses: KeyValuePairs<Expense>;
  },
  {
    expenses: Array<Expense['id']>;
  }
>;

@EnhanceNormalizer<Expense, 'expenses', number>({
  key: 'expenses',
  schema: {
    expenses: new schema.Array(new schema.Entity('expenses')),
  },
})
export class ExpenseNormalizer extends Normalizer<Expense, 'expenses', number> {}
