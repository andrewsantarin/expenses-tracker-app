import { all, call, cancelled, fork, put, takeLatest } from 'redux-saga/effects';

import { Expense } from 'core/expense';

import { listExpenses, createExpense, deleteExpense } from './expense.actions';
import { listExpenseFetch, createExpenseFetch, deleteExpenseFetch } from './expense.services';

export const listExpensesSaga = function* (action: ReturnType<typeof listExpenses.trigger>) {
  try {
    yield put(listExpenses.request(null, action.meta));
    const data: Expense[] = yield call(listExpenseFetch);
    yield put(listExpenses.success(data, null));
  } catch (error) {
    yield put(listExpenses.failure(error, null));
  } finally {
    if (yield cancelled()) {
      yield put(listExpenses.aborted(null, null));
    } else {
      yield put(listExpenses.fulfill(null, null));
    }
  }
};
export const watchListExpensesSaga = function* () {
  yield takeLatest(listExpenses, listExpensesSaga);
};

export const createExpenseSaga = function* (action: ReturnType<typeof createExpense.trigger>) {
  try {
    yield put(createExpense.request(null, action.meta));
    const data: Expense = yield call(createExpenseFetch, action.payload);
    yield put(createExpense.success(data, null));
  } catch (error) {
    yield put(createExpense.failure(error, null));
  } finally {
    if (yield cancelled()) {
      yield put(createExpense.aborted(null, null));
    } else {
      yield put(createExpense.fulfill(null, null));
    }
  }
};
export const watchCreateExpenseSaga = function* () {
  yield takeLatest(createExpense, createExpenseSaga);
};

export const deleteExpenseSaga = function* (action: ReturnType<typeof deleteExpense.trigger>) {
  try {
    yield put(deleteExpense.request(null, action.meta));
    const data: number = yield call(deleteExpenseFetch, action.meta.id);
    yield put(deleteExpense.success(data, action.meta));
  } catch (error) {
    yield put(deleteExpense.failure(error, action.meta));
  } finally {
    if (yield cancelled()) {
      yield put(deleteExpense.aborted(null, action.meta));
    } else {
      yield put(deleteExpense.fulfill(null, action.meta));
    }
  }
};
export const watchDeleteExpenseSaga = function* () {
  yield takeLatest(deleteExpense, deleteExpenseSaga);
};

export const expenseSaga = function* () {
  yield all([
    fork(watchListExpensesSaga),
    fork(watchCreateExpenseSaga),
    fork(watchDeleteExpenseSaga),
  ]);
};
