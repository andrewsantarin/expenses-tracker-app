import { HttpClient, selectData } from 'lib/fetch';
import { Expense, ExpenseValues } from 'core/expense';

const api = HttpClient({
  baseURL: '/expenses',
});

export const listExpenseFetch = async () => {
  const url = '/';
  const data = await api.get<Expense[]>(url).then(selectData);
  return data;
};

export const createExpenseFetch = async (values: Partial<ExpenseValues>) => {
  const url = '/';
  const data = await api.post<Expense>(url, values).then(selectData);
  return data;
};

export const retrieveExpenseFetch = async (id: number) => {
  const url = '/' + id;
  const data = await api.patch<Expense>(url).then(selectData);
  return data;
};

export const updateExpenseFetch = async (values: Partial<ExpenseValues>, id: number) => {
  const url = '/' + id;
  const data = await api.patch<Expense>(url).then(selectData);
  return data;
};

export const deleteExpenseFetch = async (id: number) => {
  const url = '/' + id;
  const data = await api.delete<number>(url).then(selectData);
  return data;
};
