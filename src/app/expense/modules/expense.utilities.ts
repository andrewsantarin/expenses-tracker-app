import { createPrefixedActionType as prefix } from 'lib/fetch-state';

export const actionType = (type: string) => prefix(type, '@@expense');
