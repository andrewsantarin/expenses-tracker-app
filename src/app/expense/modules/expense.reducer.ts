import { createReducer } from 'deox';
import { produce } from 'immer';

import { KeyValuePairs } from 'lib/generics';
import { createApiState, createApiStateService, ApiState } from 'lib/fetch-state';
import { createNormalizedSchema } from 'lib/schema';

import { Expense } from 'core/expense';

import { listExpenses, createExpense, updateExpense, deleteExpense, resetCreated, resetUpdated, resetDeleted } from './expense.actions';
import { ExpenseNormalizedSchema } from './expense.schema';

export type ExpenseUiState = Partial<{
  created: boolean;
  updated: boolean;
  deleted: boolean;
}>;

export type ExpenseState = {
  api: ApiState;
  schema: ExpenseNormalizedSchema;
  ui: ExpenseUiState;
};

export const updateUiState = (
  state: ExpenseUiState,
  key: keyof ExpenseUiState,
  value: ExpenseUiState[keyof ExpenseUiState]
) => ({
  ...state,
  [key]: value,
});

export const createExpenseState = (): ExpenseState => ({
  api: createApiState(),
  ui: {
    created: false,
    updated: false,
    deleted: false,
  },
  schema: createNormalizedSchema<
    Record<'expenses', KeyValuePairs<Expense>>,
    Record<'expenses', number[]>
  >(
    {
      expenses: {},
    },
    {
      expenses: [],
    },
  ),
});

export const expenseReducer = createReducer(createExpenseState(), handle => [
  // List all
  // #region
  handle(listExpenses.request, state => ({
    ...state,
    api: createApiStateService(state.api).request(),
  })),
  handle(listExpenses.success, (state, { payload }) => ({
    ...state,
    api: createApiStateService(state.api).success(),
    schema: payload,
  })),
  handle(listExpenses.failure, (state, { payload }) => ({
    ...state,
    api: createApiStateService(state.api).failure(payload),
  })),
  // #endregion

  // Create one
  // #region
  handle(createExpense.request, (state) => ({
    ...state,
    api: createApiStateService(state.api).request(),
    ui: updateUiState(state.ui, 'created', false),
  })),
  handle(createExpense.success, (state, { payload, meta }) => ({
    ...state,
    api: createApiStateService(state.api).success(),
    ui: updateUiState(state.ui, 'created', true),
    schema: produce(state.schema, (draftSchema) => {
      draftSchema.entities.expenses[payload.id] = payload;
      draftSchema.result.expenses.push(payload.id)
    }),
  })),
  handle(createExpense.failure, (state, { payload }) => ({
    ...state,
    api: createApiStateService(state.api).failure(payload),
    ui: updateUiState(state.ui, 'created', false),
  })),
  // #endregion

  // Update one
  // #region
  handle(updateExpense.request, state => ({
    ...state,
    api: createApiStateService(state.api).request(),
    ui: updateUiState(state.ui, 'updated', false),
  })),
  handle(updateExpense.success, (state, { payload, meta }) => ({
    ...state,
    api: createApiStateService(state.api).success(),
    ui: updateUiState(state.ui, 'updated', true),
    schema: produce(state.schema, (draftSchema) => {
      draftSchema.entities.expenses[meta.id] = payload;
    }),
  })),
  handle(updateExpense.failure, (state, { payload }) => ({
    ...state,
    api: createApiStateService(state.api).failure(payload),
    ui: updateUiState(state.ui, 'updated', false),
  })),
  // #endregion

  // Delete one
  // #region
  handle(deleteExpense.request, (state) => ({
    ...state,
    api: createApiStateService(state.api).request(),
    ui: updateUiState(state.ui, 'deleted', false),
  })),
  handle(deleteExpense.success, (state, { payload, meta }) => ({
    ...state,
    api: createApiStateService(state.api).success(),
    ui: updateUiState(state.ui, 'deleted', true),
    schema: produce(state.schema, (draftSchema) => {
      if (!payload) return;
      delete draftSchema.entities.expenses[meta.id];
      draftSchema.result.expenses = draftSchema.result.expenses.filter((id) => id !== meta.id);
    }),
  })),
  handle(deleteExpense.failure, (state, { payload }) => ({
    ...state,
    api: createApiStateService(state.api).failure(payload),
    ui: updateUiState(state.ui, 'deleted', false),
  })),
  // #endregion

  // UI
  // #region
  handle(resetCreated, (state) => ({
    ...state,
    ui: updateUiState(state.ui, 'created', false),
  })),
  handle(resetUpdated, (state) => ({
    ...state,
    ui: updateUiState(state.ui, 'updated', false),
  })),
  handle(resetDeleted, (state) => ({
    ...state,
    ui: updateUiState(state.ui, 'deleted', false),
  })),
  // #endregion
]);
