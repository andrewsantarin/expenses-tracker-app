import React from 'react';
import { Button, ButtonProps } from 'react-native';

export type ExpenseDeleteButtonProps = {
  id?: number;
};

export const ExpenseDeleteButton: React.FC<ButtonProps & ExpenseDeleteButtonProps> = (props) => (
  <Button {...props} />
);
