import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NativeRouter as Router, Route, Link } from 'react-router-native';

import { ExpensesListContainer, ExpenseAddFormContainer } from 'app/expense';

export const App = () => (
  <Router>
    <View style={styles.container}>
      <View style={styles.nav}>
        <Link
          to="/"
          underlayColor="#f0f4f7"
          style={styles.navItem}
        >
          <Text>List</Text>
        </Link>
        <Link
          to="/add"
          style={styles.navItem}
        >
          <Text>+ New</Text>
        </Link>
      </View>

      <Route exact path="/" component={ExpensesListContainer} />
      <Route path="/add" component={ExpenseAddFormContainer as any} />
    </View>
  </Router>
);

const styles = StyleSheet.create({
  container: {
    marginTop: 25,
    padding: 10
  },
  header: {
    fontSize: 20
  },
  nav: {
    flexDirection: "row",
    justifyContent: "space-around"
  },
  navItem: {
    flex: 1,
    alignItems: "center",
    padding: 10
  },
  subNavItem: {
    padding: 5
  },
  topic: {
    textAlign: "center",
    fontSize: 15
  }
});
