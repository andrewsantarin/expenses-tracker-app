import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export type LoadingBarProps = {
  title?: string;
};

export const LoadingBar: React.FC<LoadingBarProps> = ({ title = '' }) => (
  <View style={styles.loadingContainer}>
    <Text style={styles.loading}>
      {title}
    </Text>
  </View>
);

const styles = StyleSheet.create({
  loadingContainer: {
    backgroundColor: 'blue',
    paddingBottom: 5,
    paddingTop: 5,
  },
  loading: {
    textAlign: 'center',
    color: 'white',
  },
});
