export const CurrencyInputChangeEvent = (name: string, value: number) => (({
  type: 'number',
  target: {
    name,
    value
  }
} as any) as React.ChangeEvent<any>);
