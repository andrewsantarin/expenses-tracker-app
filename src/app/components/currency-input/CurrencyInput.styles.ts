import { StyleSheet } from 'react-native';

export const DEFAULT_STYLES = StyleSheet.create({
  input: {
    opacity: 0
  },
  text: {
    marginBottom: 0,
  },
});
