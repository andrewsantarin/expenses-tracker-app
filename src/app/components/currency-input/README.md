# CurrencyInput

Shameless copied (then adapted) from [https://github.com/larkintuckerllc/RNCurrencyInput](https://github.com/larkintuckerllc/RNCurrencyInput).

## Usage

```jsx
import React from 'react';
import { View } from 'react-native';
import { CurrencyInput } from 'components/currency-input';

export const MyComponent = () => {
  return (
    <CurrencyInput
      currency="USD"
      currencyDisplay="symbol"
      value={0}
      onChange={console.log}
      min={0}
      max={100}
    />
  );
};

```

## Props

| Prop            | Type                                                                    | Default                   | Description                                                                                                                                                                                                                                                                                                                                                  |
|-----------------|-------------------------------------------------------------------------|---------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| name            | `string`                                                                | ''                        | Optional field name when using `.onValueChange()`                                                                                                                                                                                                                                                                                                            |
| currency        | `Currency`                                                              | `'USD'`                   | See [the Mozilla docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NumberFormat) and [the Current currency & funds code list](http://www.currency-iso.org/en/home/tables/table-a1.html) for a full list.                                                                                                                |
| currencyDisplay | `CurrencyDisplay` (`'symbol' | 'code' | 'name'`)                        | `'symbol'`                | See [the Mozilla docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NumberFormat) for what these values do.                                                                                                                                                                                                              |
| value           | `number`                                                                | `0`                       |                                                                                                                                                                                                                                                                                                                                                              |
| onValueChange   | `({ type: 'number'; target: { name: string; value: number } }) => void` | `() => {}`                | Value parsed and returned by the "change" event. The value is wrapped in a very light `React.ChangeEvent` object for minimal compatibility with libraries which expect an event object and containing a numeric input value, such as [`formik`'s `handleChange()`](https://jaredpalmer.com/formik/docs/api/formik#handlechange-e-reactchangeevent-any-void). |
| onChangeText    | `(value: string) => void`                                               | `() => {}`                | Value returned by the "change" event, returned as a `string` instead of a `number`, similar to the native `` prop. Useful in form libraries which require the use of a `string` type, such as [`formik`'s `handleChange()`](https://jaredpalmer.com/formik/docs/api/formik#handlechange-e-reactchangeevent-any-void).                                        |
| onBlur          | `(event: NativeSyntheticEvent) => void`                                 | `() => {}`                |                                                                                                                                                                                                                                                                                                                                                              |
| min             | `number`                                                                | `Number.MIN_SAFE_INTEGER` | Minimum allowed value.                                                                                                                                                                                                                                                                                                                                       |
| max             | `number`                                                                | `Number.MAX_SAFE_INTEGER` | Maximum allowed value.                                                                                                                                                                                                                                                                                                                                       |
| style           | `StyleProp<TextStyle>`                                                  | `{ marginBottom: 0 }`     |                                                                                                                                                                                                                                                                                                                                                              |
