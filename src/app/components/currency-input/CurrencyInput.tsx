import React, { useCallback, useState } from 'react';
import { LayoutChangeEvent, StyleProp, StyleSheet, Text, TextStyle, TextInput, TextInputProps } from 'react-native';
import isFunction from 'lodash/isFunction';

import { DEFAULT_STYLES as styles } from './CurrencyInput.styles';
import { Currency, CurrencyDisplay } from './CurrencyInput.types';
import { CurrencyInputChangeEvent } from './CurrencyInput.utilities';
import { VALID } from './CurrencyInput.constants';

export type CurrencyInputProps = Partial<{
  name: string;
  currency: Currency;
  currencyDisplay: CurrencyDisplay;
  value: number;
  onValueChange: (value: number) => void;
  onChangeText: (value: string) => void;
  onBlur: TextInputProps['onBlur'];
  min: number;
  max: number;
  style: StyleProp<TextStyle>;
}>;

export const CurrencyInput: React.FC<CurrencyInputProps> = ({
  name = '',
  currency = 'USD',
  currencyDisplay = 'symbol', 
  value = 0,
  onValueChange = () => {},
  onChangeText = () => {},
  onBlur = () => {},
  min = Number.MIN_SAFE_INTEGER,
  max = Number.MAX_SAFE_INTEGER,
  style = { marginBottom: 0 },
}) => {
  const valueAbsTrunc = Math.trunc(Math.abs(value));
  if (value !== valueAbsTrunc || !Number.isFinite(value) || Number.isNaN(value)) {
    throw new Error(`invalid value property`);
  }

  const [ inputHeight, setInputHeight ] = useState(0);
  const [ inputWidth, setInputWidth ] = useState(0);

  const handleChangeText = useCallback((text: string) => {
    if (text === '') {
      const nil = 0;
      isFunction(onValueChange) && onValueChange(nil);
      isFunction(onChangeText) && onChangeText(nil.toString());
      return;
    }
    if (!VALID.test(text)) {
      return;
    }
    const nextValue = parseInt(text, 10);
    if (nextValue < min) {
      return;
    }
    if (nextValue > max) {
      return;
    }
    isFunction(onValueChange) && onValueChange(nextValue);
    isFunction(onChangeText) && onChangeText(text);
  }, []);

  const handleLayout = useCallback((event: LayoutChangeEvent) => {
    const { height, width } = event.nativeEvent.layout;
    setInputHeight(height);
    setInputWidth(width);
  }, []);

  const { marginBottom } = StyleSheet.flatten(style);
  const valueInput = value === 0 ? '' : value.toString();
  const valueDisplay = (value / 100).toLocaleString('en-US', {
    style: 'currency',
    currency: currency,
    currencyDisplay: currencyDisplay,
  });

  return (
    <>
      <Text
        onLayout={handleLayout}
        style={[
          style,
          styles.text
        ]}
      >
        {valueDisplay}
      </Text>
      <TextInput
        contextMenuHidden
        keyboardType="numeric"
        onChangeText={handleChangeText}
        onBlur={onBlur}
        value={valueInput}
        style={[
          styles.input,
          {
            height: inputHeight,
            marginBottom,
            marginTop: -1 * inputHeight,
            width: inputWidth
          }
        ]}
      />
    </>
  );
};
