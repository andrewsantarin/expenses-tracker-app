import { createStore, applyMiddleware, DeepPartial } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools as compose } from 'redux-devtools-extension';

import { rootReducer, RootState } from './root.reducer';
import { rootSaga } from './root.sagas';

export const configureStore = (preloadedState: DeepPartial<RootState> = {}) => {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    rootReducer,
    preloadedState,
    compose(
      applyMiddleware(
        sagaMiddleware,
      )
    )
  );

  sagaMiddleware.run(rootSaga);

  return store;
};
