export type Expense = {
  id: number;
  currency: string;
  amount: number;
};

export type ExpenseValues = Pick<Expense, 'currency' | 'amount'>;
